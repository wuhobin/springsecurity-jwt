package com.wuhobin.springsecurity.mapper;

import com.wuhobin.springsecurity.dataobject.UserDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* <p>
 *  Mapper 接口
 * </p>
*
* @author wuhongbin
* @since 2023-01-11
*/

public interface UserMapper extends BaseMapper<UserDO> {

}
