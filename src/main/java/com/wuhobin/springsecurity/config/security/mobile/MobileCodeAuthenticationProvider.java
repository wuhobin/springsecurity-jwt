package com.wuhobin.springsecurity.config.security.mobile;

import com.wuhobin.springsecurity.cache.SmsCodeCache;
import com.wuhobin.springsecurity.constant.MessageConstant;
import com.wuhobin.springsecurity.service.UserDetailService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * @author wuhongbin
 * @description: 类比于DaoAuthenticationProvider
 * @datetime 2023/01/17 16:02
 */
@Slf4j
public class MobileCodeAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private SmsCodeCache smsCodeCache;


    private UserDetailService userDetailService;

    public MobileCodeAuthenticationProvider(UserDetailService userDetailService) {
        this.userDetailService = userDetailService;
    }


    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        MobileCodeAuthenticationToken authenticationToken = (MobileCodeAuthenticationToken) authentication;
        String mobile = (String) authenticationToken.getPrincipal();
        UserDetails user = userDetailService.loadUserByMobile(mobile);
        log.info("MobileCodeAuthenticationProvider  -----> 执行loadUserByMobile");
        if (ObjectUtils.isEmpty(user)) {
            throw new InternalAuthenticationServiceException(MessageConstant.USER_NO_REGISTER);
        }
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String inputCode = request.getParameter("code");
        log.info("MobileCodeAuthenticationProvider  -----> code = {}, password = {}", inputCode, user.getPassword());
        if (!user.getPassword().equals(inputCode)) {
            log.info("MobileCodeAuthenticationProvider  -----> 验证码错误");
            smsCodeCache.addVerifyCodeErr(mobile);
            log.info("MobileCodeAuthenticationProvider  ----->  用户登录-验证码输入错误,mobile={}", mobile);
            throw new BadCredentialsException(MessageConstant.VERIFY_CODE_ERROR);
        }
        MobileCodeAuthenticationToken token = new MobileCodeAuthenticationToken(user, user.getAuthorities());
        token.setDetails(authenticationToken.getDetails());
        return token;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return MobileCodeAuthenticationToken.class.isAssignableFrom(authentication);
    }
}
