package com.wuhobin.springsecurity.config.security;

import cn.hutool.json.JSONUtil;
import com.wuhobin.springsecurity.common.api.CommonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author wuhongbin
 * @description: spring security 自定义登录失败时的处理逻辑
 * @datetime 2023/01/12 16:31
 */
@Component
@Slf4j
public class AuthenticationFailure implements AuthenticationFailureHandler {
    @Override
    public void onAuthenticationFailure(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
        httpServletRequest.setCharacterEncoding("UTF-8");
        httpServletResponse.setCharacterEncoding("UTF-8");
        httpServletResponse.setContentType("application/json;charset=utf-8");
        String msg = null;
        log.info("登录失败拉 ");
        if (e instanceof BadCredentialsException){
            msg = "用户名或者密码错误";
        } else {
            msg = e.getMessage();
        }
        httpServletResponse.getWriter().write(JSONUtil.parseObj(CommonResult.failed(msg)).toString());

    }
}
