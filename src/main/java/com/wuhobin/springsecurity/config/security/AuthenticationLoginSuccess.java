package com.wuhobin.springsecurity.config.security;

import cn.hutool.json.JSONUtil;
import com.wuhobin.springsecurity.common.api.CommonResult;
import com.wuhobin.springsecurity.common.api.ResultCode;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author wuhongbin
 * @description: 登录成功
 * @datetime 2023/01/13 11:21
 */
@Component
public class AuthenticationLoginSuccess implements AuthenticationSuccessHandler  {
    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        httpServletRequest.setCharacterEncoding("UTF-8");
        httpServletResponse.setCharacterEncoding("UTF-8");
        httpServletResponse.setContentType("application/json;charset=utf-8");
        httpServletResponse.getWriter().write(JSONUtil.parseObj(CommonResult.success(authentication.getDetails())).toString());
    }
}
