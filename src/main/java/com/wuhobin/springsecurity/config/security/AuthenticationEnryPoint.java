package com.wuhobin.springsecurity.config.security;

import cn.hutool.json.JSONUtil;
import com.wuhobin.springsecurity.common.api.CommonResult;
import com.wuhobin.springsecurity.constant.MessageConstant;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;
import sun.plugin2.message.Message;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author wuhongbin
 * @description: spring security 未登录处理逻辑
 * @datetime 2023/01/12 16:21
 */
@Component
public class AuthenticationEnryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
        httpServletRequest.setCharacterEncoding("UTF-8");
        httpServletResponse.setCharacterEncoding("UTF-8");
        httpServletResponse.setContentType("application/json;charset=utf-8");
        String msg = null;
        if (e instanceof BadCredentialsException){
            msg = MessageConstant.VERIFY_CODE_ERROR;
        } else if (e instanceof InternalAuthenticationServiceException){
            msg = e.getMessage();
        } else if (e instanceof InsufficientAuthenticationException){
            msg = MessageConstant.MSG_NOT_LOGIN;
        }else {
            msg = "未登录，无法访问相应资源！";
        }
        httpServletResponse.getWriter().write(JSONUtil.parseObj(CommonResult.success(msg)).toString());
    }
}


/*
* if (e instanceof BadCredentialsException){
            msg = "用户名或者密码错误";
        } else {
            msg = e.getMessage();
        }
* BadCredentialsException 我们在认证的地方 如果抛出了这个异常的话 我们就可以在这个 处理器里面拿到相应的错误信息 返回给前端
*
* */
