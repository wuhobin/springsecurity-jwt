package com.wuhobin.springsecurity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.TemplateType;
import com.baomidou.mybatisplus.generator.config.converts.MySqlTypeConvert;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.DbColumnType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.Collections;

/**
 * mybatis-plus代码生成器
 *
 * @author 刘博
 */
public class NewCodeGeneratorPlus {

    private static String jdbcUrl = "jdbc:mysql://180.184.194.131:3306/zx-bank-admin?useUnicode=true&characterEncoding=UTF-8";
    private static String jdbcUsername = "root";
    private static String jdbcPassword = "1qazXSW@";

    /**
     * 父级包名配置
     */
    private static String parentPackage = "com.weijuju.iag.zxbankadmin.domain";

    /**
     * 项目业务module
     */
    private static String moduleName = "zx-bank-admin-domain";

    /**
     * 生成代码的 @author 值
     */
    private static String author = "whb";

    /**
     * 项目地址[改为自己本地项目位置]
     */
    private static String projectPath = "D:/code/after-end/zx-bank-admin";

    /**
     * mapper.xml模板引擎
     */
    private static String mapperTemplatePath = "/templates/mapper.xml.ftl";

    /**
     * vo模板引擎
     */
    private static String entityTemplatePath = "/templates/vo.java.ftl";

    /**
     * do模板引擎
     */
    private static String dataObjectTemplatePath = "/templates/dataobject.java.ftl";

    /**
     * java mapper模板引擎
     */
    private static String javaMapperTemplatePath = "/templates/mapper.java.ftl";

    /**
     * vo生成目录
     */
    private static String voPath = "/src/main/java/com/weijuju/iag/cebbankadmin/domain/vo";

    /**
     * do生成目录
     */
    private static String doPath = "/src/main/java/com/weijuju/iag/cebbankadmin/domain/dataobject";

    /**
     * java Mapper生成目录
     */
    private static String mapperPath = "/src/main/java/com/weijuju/iag/cebbankadmin/domain/mapper";

    /**
     * 要生成代码的表名配置
     */
    private static String[] tables = {
            //"t_project_config"
            "t_user_info"
    };


    private static final DataSourceConfig.Builder DATA_SOURCE_CONFIG = new DataSourceConfig
            .Builder(jdbcUrl, jdbcUsername, jdbcPassword)
            .typeConvert(new MySqlTypeConvert() {
                @Override
                public DbColumnType processTypeConvert(GlobalConfig globalConfig, String fieldType) {
                    /**
                     *   tinyint转换成Boolean
                     */
                    if (fieldType.toLowerCase().contains("tinyint")) {
                        return DbColumnType.BOOLEAN;
                    }
                    /**
                     *  将数据库中datetime,date,timestamp转换成date
                     */
                    if (fieldType.toLowerCase().contains("datetime") || fieldType.toLowerCase().contains("date") || fieldType.toLowerCase().contains("timestamp")) {
                        return DbColumnType.DATE;
                    }
                    return (DbColumnType) super.processTypeConvert(globalConfig, fieldType);
                }
            });


    /**
     * <p>
     * MySQL 生成演示
     * </p>
     */
    public static void main(String[] args) {
        //1、配置数据源
        FastAutoGenerator.create(DATA_SOURCE_CONFIG)
                //2、全局配置
                .globalConfig(builder -> {
                    builder
                            .outputDir(projectPath + "/" + moduleName + "/src/main/java")  //指定输出目录
                            .author(author)    // 作者名
                            .disableOpenDir()  // 禁止打开输出目录
                            .dateType(DateType.ONLY_DATE) //时间策略
                            .commentDate("yyyy-MM-dd") // 注释日期
                            .enableSwagger();  //开启 swagger 模式
                })  //3、包配置
                .packageConfig(builder -> {
                    builder
                            .parent(parentPackage)
                            .entity("dataobject")   // Entity 包名
                            .mapper("mapper")       // Mapper 包名
                            .service("service")    //  Service 包名
                            .serviceImpl("service.impl") //Service Impl 包名
                            .pathInfo(Collections.singletonMap(OutputFile.xml, projectPath + "/" + moduleName + "/src/main/resources/sqlmap/mapper"));
                })  // 4. 注入配置
                //.injectionConfig(consumer -> {
                //    consumer
                //            .beforeOutputFile((tableInfo, objectMap) -> {   // xml输出
                //                objectMap.put(projectPath + "/" + moduleName + "/src/main/resources/sqlmap/mapper"
                //                        + "/" + tableInfo.getEntityName() + "Mapper" + StringPool.DOT_XML, mapperTemplatePath);
                //            })
                //            .beforeOutputFile((tableInfo, objectMap) -> {   // VO输出
                //                objectMap.put(projectPath + "/" + moduleName + voPath
                //                        + "/" + tableInfo.getEntityName() + "VO" + StringPool.DOT_JAVA, entityTemplatePath);
                //            })
                //            .beforeOutputFile((tableInfo, objectMap) -> {   // DO输出
                //                objectMap.put(projectPath + "/" + moduleName + doPath
                //                        + "/" + tableInfo.getEntityName() + "DO" + StringPool.DOT_JAVA, dataObjectTemplatePath);
                //            })
                //            .beforeOutputFile((tableInfo, objectMap) -> {   // Mapper输出
                //                objectMap.put(projectPath + "/" + moduleName + mapperPath
                //                        + "/" + tableInfo.getEntityName() + "Mapper" + StringPool.DOT_JAVA, javaMapperTemplatePath);
                //            });
                //
                //})
                .strategyConfig(builder -> {
                    builder
                            .enableCapitalMode()    //开启大写命名
                            .enableSkipView()   //创建实体类的时候跳过视图
                            .addInclude(tables)  // 设置需要生成的数据表名
                            .addTablePrefix("t_")  //设置 过滤 表的前缀
                            .entityBuilder()       // Entity 策略配置
                            .enableLombok()       //开启 lombok 模型
                            .idType(IdType.AUTO)    // 全局主键类型
                            .formatFileName("%sDO")
                            .enableRemoveIsPrefix()  //开启 Boolean 类型字段移除 is 前缀
                            .naming(NamingStrategy.underline_to_camel)  //数据库表映射到实体的命名策略
                            .columnNaming(NamingStrategy.underline_to_camel)  //数据库表字段映射到实体的命名策略
                            .controllerBuilder()   // Controller 策略配置
                            .enableRestStyle()
                            .formatFileName("%sController")
                            .serviceBuilder()
                            .formatServiceFileName("%sService")
                            .formatServiceImplFileName("%sServiceImp")
                            .mapperBuilder()
                            .enableBaseColumnList()
                            .enableBaseResultMap()
                            .formatMapperFileName("%sMapper")
                            .formatXmlFileName("%sMapper");
                })
                .templateConfig(builder -> {
                    builder
                            .disable(TemplateType.ENTITY)
                            .entity("/templates/dataobject.java")
                            //.mapperXml(null)
                            //.entity(null)
                            //.service(null)
                            //.serviceImpl(null)
                            .controller(null);// 不生成controller
                })
                .templateEngine(new FreemarkerTemplateEngine())
                .execute();

//        AutoGenerator mpg = new AutoGenerator();
//        // 选择 freemarker 引擎，默认 Veloctiy
//        mpg.setTemplateEngine(new FreemarkerTemplateEngine());
//        // 全局配置
//        // 全局配置
//        GlobalConfig gc = new GlobalConfig();
//        gc.setOutputDir(projectPath + moduleName + "/src/main/java");
//        gc.setAuthor(author);
//        gc.setBaseResultMap(true);
//        gc.setBaseColumnList(true);
//        // 是否覆盖同名文件，默认是false
//        gc.setFileOverride(true);
//        // 生成完毕后是否打开输出目录
//        gc.setOpen(false);
//        // 为true时生成entity将继承Model类，单类即可完成基于单表的业务逻辑操作，按需开启
//        gc.setActiveRecord(false);
//        // XML 二级缓存
//        gc.setEnableCache(false);
//        gc.setDateType(DateType.ONLY_DATE);
//        // 自定义文件命名，注意 %s 会自动填充表实体属性！
////        gc.setEntityName("%sDo");
//        gc.setMapperName("%sMapper");
//        gc.setXmlName(gc.getMapperName());
//        gc.setServiceName("%sService");
//        gc.setServiceImplName("%sServiceImpl");
//        //添加swagger注解
//
//        gc.setSwagger2(true);
//        mpg.setGlobalConfig(gc);
//        // 数据源配置,url、用户名、密码
//        DataSourceConfig dsc = new DataSourceConfig();
//        dsc.setDbType(DbType.MYSQL);
//        dsc.setDriverName("com.mysql.cj.jdbc.Driver");
//        dsc.setUsername(jdbcUsername);
//        dsc.setPassword(jdbcPassword);
//        dsc.setUrl(jdbcUrl);
        //生成的实体类中日期类型的变量对应为Date类型
        //dsc.setTypeConvert(new MySqlTypeConvert() {
        //    @Override
        //    public DbColumnType processTypeConvert(GlobalConfig globalConfig, String fieldType) {
        //        /**
        //         *   tinyint转换成Boolean
        //         */
        //        if (fieldType.toLowerCase().contains("tinyint")) {
        //            return DbColumnType.BOOLEAN;
        //        }
        //        /**
        //         *  将数据库中datetime,date,timestamp转换成date
        //         */
        //        if (fieldType.toLowerCase().contains("datetime") || fieldType.toLowerCase().contains("date") || fieldType.toLowerCase().contains("timestamp")) {
        //            return DbColumnType.DATE;
        //        }
        //        return (DbColumnType) super.processTypeConvert(globalConfig, fieldType);
        //    }
        //});
//        mpg.setDataSource(dsc);
//
//
//        // 自定义配置
//        InjectionConfig cfg = new InjectionConfig() {
//            @Override
//            public void initMap() {
//                // to do nothing
//            }
//        };
//        // 自定义输出配置
//        List<FileOutConfig> focList = new ArrayList<>();
//
//        //Mapper.xml输出
//        focList.add(new FileOutConfig(mapperTemplatePath) {
//            @Override
//            public String outputFile(TableInfo tableInfo) {
//                // 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化！！
//                return projectPath + "/" + moduleName + "/src/main/resources/sqlmap/mapper"
//                        + "/" + tableInfo.getEntityName() + "Mapper" + StringPool.DOT_XML;
//            }
//        });
//
//        //vo输出
//        focList.add(new FileOutConfig(entityTemplatePath) {
//            @Override
//            public String outputFile(TableInfo tableInfo) {
//                // 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化！！
//                return projectPath + "/" + moduleName + voPath
//                        + "/" + tableInfo.getEntityName() + "VO" + StringPool.DOT_JAVA;
//            }
//        });
//
//        //DO输出
//        focList.add(new FileOutConfig(dataObjectTemplatePath) {
//            @Override
//            public String outputFile(TableInfo tableInfo) {
//                // 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意do xml 的名称会跟着发生变化！！
//                return projectPath + "/" + moduleName + doPath
//                        + "/" + tableInfo.getEntityName() + "DO" + StringPool.DOT_JAVA;
//            }
//        });
//
//        // mapper输出
//        focList.add(new FileOutConfig(javaMapperTemplatePath) {
//            @Override
//            public String outputFile(TableInfo tableInfo) {
//                // 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化！！
//                return projectPath + "/" + moduleName + mapperPath
//                        + "/" + tableInfo.getEntityName() + "Mapper" + StringPool.DOT_JAVA;
//            }
//        });
//        cfg.setFileOutConfigList(focList);
//        mpg.setCfg(cfg);
//        // 配置模板
//        TemplateConfig templateConfig = new TemplateConfig();
//        //控制 不生成 controller
//        templateConfig.setXml(null);
//        // 是否要重新生成entity,service及实现
//        templateConfig.setEntity(null);
//        templateConfig.setController(null);
//        templateConfig.setMapper(null);
//        mpg.setTemplate(templateConfig);
//        mpg.setTemplateEngine(new FreemarkerTemplateEngine());
//
//        // 策略配置
//        StrategyConfig strategy = new StrategyConfig();
//        // 此处可以修改为您的表前缀,可去掉自己想去掉的表前缀
//        strategy.setTablePrefix("t_");
//        // 表名生成策略,驼峰命名法
//        strategy.setNaming(NamingStrategy.underline_to_camel);
//        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
//        // 需要生成的表
//        strategy.setInclude(tables);
//        // 排除生成的表
//        //strategy.setExclude(new String[]{"activity_log","t_log","user_activity"});
//        //开启实体类的lombok注解
//        strategy.setEntityLombokModel(true);
//        strategy.setRestControllerStyle(false);
//        // 在实体类中移除is前缀
//        strategy.setEntityBooleanColumnRemoveIsPrefix(true);
//        mpg.setStrategy(strategy);
//        // 包配置
//        PackageConfig pc = new PackageConfig();
//        //这里写该目录下的包名
//        pc.setParent(parentPackage);
//        pc.setEntity("dataobject");
//        pc.setMapper("mapper");
//        //服务层的接口以及实现类
//        pc.setService("service");
//        pc.setController("vo");
//        pc.setServiceImpl("service.impl");
//        mpg.setPackageInfo(pc);
//        // 执行生成
//        mpg.execute();
//        System.out.println("生成成功...");
    }
}
