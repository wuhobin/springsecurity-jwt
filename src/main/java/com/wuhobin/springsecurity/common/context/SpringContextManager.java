package com.wuhobin.springsecurity.common.context;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * 获取 Spring 上下文组件
 *
 * @author hufeng
 * @date 2015.10.22
 * @version 1.4
 */
@Component
public class SpringContextManager implements ApplicationContextAware {

	private static ApplicationContext ctx;


	private static final String PRODUCT = "product";
	private static final String DEV = "dev";
	private static final String TEST = "test";
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		ctx = applicationContext;
	}

	/**
	 * 得到当前spring上下文
	 * @return
	 */
	public static ApplicationContext getApplicationContext() {
		return ctx;
	}


	/**
	 * 获得bean
	 * @param requiredType 类型
	 * @param <T> 类型
	 * @return spring 容器中的对象
	 */
	public static <T> T getBean(Class<T> requiredType){
		return getApplicationContext().getBean(requiredType);
	}

	/**
	 * 获得SpringBean
	 * @param name Bean 名字
	 * @param requiredType 类型
	 * @param <T> 强转换类型
	 * @return 返回结果
	 */
	public static <T> T getBean(String name, Class<T> requiredType){
		return getApplicationContext().getBean(name,requiredType);
	}



	/**
	 * 获取当前环境
	 */
	public static String getActiveProfile() {
		return ctx.getEnvironment().getActiveProfiles()[0];
	}


	public static Boolean isProduct(){
	   return StringUtils.equals(getActiveProfile(), PRODUCT);
	}


	public static Boolean isTest(){
		return 	StringUtils.equals(getActiveProfile(), TEST);
	}


	public static Boolean isDev(){
		return 	StringUtils.equals(getActiveProfile(), DEV);
	}


}
