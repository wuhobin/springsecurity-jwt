package com.wuhobin.springsecurity.common.exception;


import com.wuhobin.springsecurity.common.api.ResultCode;

/**
 *
 * @author admin
 */
public class BusinessException extends RuntimeException {

    private Long code;

    public BusinessException(ResultCode resultCode) {
        super(resultCode.getMessage());
        this.code = resultCode.getCode();
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }
}
