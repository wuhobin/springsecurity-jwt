package com.wuhobin.springsecurity.common.api;

/**
 * 枚举了一些常用API操作码
 *
 * @author wuhongbin
 */
public enum ResultCode implements IErrorCode {

    /**
     * 返回信息
     */
    SUCCESS(200, "操作成功"),
    FAILED(-500, "系统异常"),
    VALIDATE_FAILED(400, "参数检验失败"),
    UNAUTHORIZED(401, "未登录，请先登录"),
    FORBIDDEN(403, "无权限,请先登录"),
    LOGOUT_OUT(406, "退出登录"),
    ACCOUNT_ERR(405, "用户名或者密码错误"),
    LOGIN_FAIL(407,"登录失败，token生成失败"),
    PLEASE_TRY_AGAIN_LATER(-600, "请稍后重试"),


    SEND_VERIFY_ERR(-5004, "验证码错误"),
    PLEASE_TRY_LATER(-5002, "还未到限制发送时间，稍后再试"),
    ASTRICT_LOGIN(-702, "验证码连续错误三次，限制登录两小时"),
    SEND_VERIFY_CODE_ERR(-5003, "生成验证码异常，请稍后再试");




    private long code;
    private String message;

    ResultCode(long code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public long getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
