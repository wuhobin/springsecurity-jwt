package com.wuhobin.springsecurity.service.impl;

import com.wuhobin.springsecurity.cache.SmsCodeCache;
import com.wuhobin.springsecurity.constant.MessageConstant;
import com.wuhobin.springsecurity.service.UserDetailService;
import com.wuhobin.springsecurity.service.UserService;
import com.wuhobin.springsecurity.vo.UserVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * @author wuhongbin
 * @description: UserDetailServiceImpl
 * @datetime 2023/01/12 16:58
 */
@Service
@Slf4j
public class UserDetailServiceImpl implements UserDetailService {

    @Autowired
    private UserService userService;

    @Autowired
    private SmsCodeCache smsCodeCache;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        log.info("UserDetailServiceImpl 用户用户名密码登录 username->{}",username);
        UserVO userVO = userService.selectUserByName(username);
        if (ObjectUtils.isEmpty(userVO)){
            throw new InternalAuthenticationServiceException(MessageConstant.USER_NO_REGISTER);
        }

        return userVO;
    }

    @Override
    public UserDetails loadUserByMobile(String mobile) {
        log.info("UserDetailServiceImpl 用户手机号验证码登录 mobile->{}",mobile);
        UserVO userVO = userService.selectUserByMobile(mobile);
        if (ObjectUtils.isEmpty(userVO)){
            throw new InternalAuthenticationServiceException(MessageConstant.USER_NO_REGISTER);
        }
        String verifyCode = smsCodeCache.getVerifyCode(mobile);
        if (StringUtils.isBlank(verifyCode)){
            throw new InternalAuthenticationServiceException(MessageConstant.VERIFY_CODE_NOT_CLICK);
        }
        userVO.setPassword(verifyCode);
        return userVO;
    }
}
