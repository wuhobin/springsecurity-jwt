package com.wuhobin.springsecurity.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wuhobin.springsecurity.service.SmsService;
import com.zhenzi.sms.ZhenziSmsClient;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * @author wuhongbin
 * @description: 手机号验证码
 * @datetime 2023/01/17 11:21
 */
@SuppressWarnings("all")
@Service
@Slf4j
public class SmsServiceImpl implements SmsService {

    @Value("${zhenzi.apiUrl}")
    private String apiUrl;

    @Value("${zhenzi.apiId}")
    private String apiId;

    @Value("${zhenzi.appSecret}")
    private String appSecret;

    @Value("${zhenzi.templateId}")
    private String templateId;

    @Override
    public String sendCode(String mobile) {
        String code;
        try {
            ZhenziSmsClient client = new ZhenziSmsClient(apiUrl, apiId, appSecret);
            code = RandomStringUtils.randomNumeric(6);
            Map<String, Object> params = new HashMap<>(2);
            params.put("number", mobile);
            params.put("templateId", templateId);
            String[] templateParams = new String[2];
            templateParams[0] = code;
            templateParams[1] = "3分钟";
            params.put("templateParams", templateParams);
            String result = client.send(params);
            JSONObject jsonObject = JSON.parseObject(result);
            log.info("榛子短信验证码 反参 jsonObject={}",jsonObject.toString());
            if (jsonObject.getIntValue("code") == 0){
               return code;
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
