package com.wuhobin.springsecurity.service.impl;

import com.wuhobin.springsecurity.dataobject.UserDO;
import com.wuhobin.springsecurity.mapper.UserMapper;
import com.wuhobin.springsecurity.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wuhobin.springsecurity.util.BeanCopyUtil;
import com.wuhobin.springsecurity.vo.UserVO;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuhongbin
 * @since 2023-01-11
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, UserDO> implements UserService {

    @Override
    public UserVO selectUserByName(String username) {
        UserDO one = lambdaQuery().eq(UserDO::getUsername, username).last("limit 0,1").one();
        return BeanCopyUtil.copy(one,UserVO.class);
    }

    @Override
    public UserVO selectUserByMobile(String mobile) {
        UserDO one = lambdaQuery().eq(UserDO::getMobile, mobile).last("limit 0,1").one();
        return BeanCopyUtil.copy(one,UserVO.class);
    }
}
