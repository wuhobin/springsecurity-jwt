package com.wuhobin.springsecurity.service;

import com.wuhobin.springsecurity.dataobject.UserDO;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wuhobin.springsecurity.vo.UserVO;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wuhongbin
 * @since 2023-01-11
 */
public interface UserService extends IService<UserDO> {

    /**
     * 根据username查找
     * @param username
     * @return
     */
    UserVO selectUserByName(String username);



    /**
     * 根据mobile查找
     * @param mobile
     * @return
     */
    UserVO selectUserByMobile(String mobile);
}
