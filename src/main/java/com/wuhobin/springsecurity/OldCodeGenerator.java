//package com.wuhobin.springsecurity;
//
//
//
//import com.baomidou.mybatisplus.annotation.DbType;
//import com.baomidou.mybatisplus.core.toolkit.StringPool;
//import com.baomidou.mybatisplus.generator.AutoGenerator;
//import com.baomidou.mybatisplus.generator.InjectionConfig;
//import com.baomidou.mybatisplus.generator.config.*;
//import com.baomidou.mybatisplus.generator.config.converts.MySqlTypeConvert;
//import com.baomidou.mybatisplus.generator.config.po.TableInfo;
//import com.baomidou.mybatisplus.generator.config.rules.DateType;
//import com.baomidou.mybatisplus.generator.config.rules.DbColumnType;
//import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
//import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * mybatis-plus代码生成器
// *
// * @author 刘博
// */
//public class OldCodeGenerator {
//
//    private static String jdbcUrl = "jdbc:mysql://rm-bp1j8k9s2330jz5jdyo.mysql.rds.aliyuncs.com/security_jwt?useUnicode=true&characterEncoding=UTF-8";
//    private static String jdbcUsername = "root";
//    private static String jdbcPassword = "Whb18772916901";
//
//    /**
//     * 父级包名配置
//     */
//    private static String parentPackage = "com.wuhobin.springsecurity";
//
//    /**
//     * 项目业务module
//     */
//    private static String moduleName = "springsecurity_jwt";
//
//    /**
//     * 生成代码的 @author 值
//     */
//    private static String author = "wuhongbin";
//
//    /**
//     * 项目地址
//     */
//    private static String projectPath = "D:/idea_wuhongbin/springboot_study/";
//
//    /**
//     * mapper.xml模板引擎
//     */
//    private static String mapperTemplatePath = "/templates/mapper.xml.ftl";
//
//    /**
//     * vo模板引擎
//     */
//    private static String entityTemplatePath = "/templates/vo.java.ftl";
//
//    /**
//     * do模板引擎
//     */
//    private static String dataObjectTemplatePath = "/templates/dataobject.java.ftl";
//
//    /**
//     * java mapper模板引擎
//     */
//    private static String javaMapperTemplatePath = "/templates/mapper.java.ftl";
//
//    /**
//     * vo生成目录
//     */
//    private static String voPath = "/src/main/java/com/wuhobin/springsecurity/vo";
//
//    /**
//     * do生成目录
//     */
//    private static String doPath = "/src/main/java/com/wuhobin/springsecurity/dataobject";
//
//    /**
//     * java Mapper生成目录
//     */
//    private static String mapperPath = "/src/main/java/com/wuhobin/springsecurity/mapper";
//
//    /**
//     * 要生成代码的表名配置
//     */
//    private static String[] tables = {
//            "t_user"
//    };
//
//    /**
//     * <p>
//     * MySQL 生成演示
//     * </p>
//     */
//    public static void main(String[] args) {
//        AutoGenerator mpg = new AutoGenerator();
//        // 选择 freemarker 引擎，默认 Veloctiy
//        mpg.setTemplateEngine(new FreemarkerTemplateEngine());
//        // 全局配置
//        // 全局配置
//        GlobalConfig gc = new GlobalConfig();
//        gc.setOutputDir(projectPath + moduleName + "/src/main/java");
//        gc.setAuthor(author);
//        gc.setBaseResultMap(true);
//        gc.setBaseColumnList(true);
//        // 是否覆盖同名文件，默认是false
//        gc.setFileOverride(true);
//        // 生成完毕后是否打开输出目录
//        gc.setOpen(false);
//        // 为true时生成entity将继承Model类，单类即可完成基于单表的业务逻辑操作，按需开启
//        gc.setActiveRecord(false);
//        // XML 二级缓存
//        gc.setEnableCache(false);
//        gc.setDateType(DateType.ONLY_DATE);
//        // 自定义文件命名，注意 %s 会自动填充表实体属性！
////        gc.setEntityName("%sDo");
//        gc.setMapperName("%sMapper");
//        gc.setXmlName(gc.getMapperName());
//        gc.setServiceName("%sService");
//        gc.setServiceImplName("%sServiceImpl");
//        //添加swagger注解
//        gc.setSwagger2(true);
//        mpg.setGlobalConfig(gc);
//        // 数据源配置,url、用户名、密码
//        DataSourceConfig dsc = new DataSourceConfig();
//        dsc.setDbType(DbType.MYSQL);
//        dsc.setDriverName("com.mysql.jdbc.Driver");
//        dsc.setUsername(jdbcUsername);
//        dsc.setPassword(jdbcPassword);
//        dsc.setUrl(jdbcUrl);
//        //生成的实体类中日期类型的变量对应为Date类型
//        dsc.setTypeConvert(new MySqlTypeConvert() {
//            @Override
//            public DbColumnType processTypeConvert(GlobalConfig globalConfig, String fieldType) {
//                /**
//                 *   tinyint转换成Boolean
//                 */
//                if (fieldType.toLowerCase().contains("tinyint")) {
//                    return DbColumnType.BOOLEAN;
//                }
//                /**
//                 *  将数据库中datetime,date,timestamp转换成date
//                 */
//                if (fieldType.toLowerCase().contains("datetime") || fieldType.toLowerCase().contains("date") || fieldType.toLowerCase().contains("timestamp")) {
//                    return DbColumnType.DATE;
//                }
//                return (DbColumnType) super.processTypeConvert(globalConfig, fieldType);
//            }
//        });
//        mpg.setDataSource(dsc);
//
//
//        // 自定义配置
//        InjectionConfig cfg = new InjectionConfig() {
//            @Override
//            public void initMap() {
//                // to do nothing
//            }
//        };
//        // 自定义输出配置
//        List<FileOutConfig> focList = new ArrayList<>();
//
//        //Mapper.xml输出
//        focList.add(new FileOutConfig(mapperTemplatePath) {
//            @Override
//            public String outputFile(TableInfo tableInfo) {
//                // 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化！！
//                return projectPath + "/" + moduleName + "/src/main/resources/mapper"
//                        + "/" + tableInfo.getEntityName() + "Mapper" + StringPool.DOT_XML;
//            }
//        });
//
//        //vo输出
//        focList.add(new FileOutConfig(entityTemplatePath) {
//            @Override
//            public String outputFile(TableInfo tableInfo) {
//                // 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化！！
//                return projectPath + "/" + moduleName + voPath
//                        + "/" + tableInfo.getEntityName() + "VO" + StringPool.DOT_JAVA;
//            }
//        });
//
//        //DO输出
//        focList.add(new FileOutConfig(dataObjectTemplatePath) {
//            @Override
//            public String outputFile(TableInfo tableInfo) {
//                // 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意do xml 的名称会跟着发生变化！！
//                return projectPath + "/" + moduleName + doPath
//                        + "/" + tableInfo.getEntityName() + "DO" + StringPool.DOT_JAVA;
//            }
//        });
//
//        // mapper输出
//        focList.add(new FileOutConfig(javaMapperTemplatePath) {
//            @Override
//            public String outputFile(TableInfo tableInfo) {
//                // 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化！！
//                return projectPath + "/" + moduleName + mapperPath
//                        + "/" + tableInfo.getEntityName() + "Mapper" + StringPool.DOT_JAVA;
//            }
//        });
//        cfg.setFileOutConfigList(focList);
//        mpg.setCfg(cfg);
//        // 配置模板
//        TemplateConfig templateConfig = new TemplateConfig();
//        //控制 不生成 controller
//        templateConfig.setXml(null);
//        // 是否要重新生成entity,service及实现
//        templateConfig.setEntity(null);
//        templateConfig.setController(null);
//        templateConfig.setMapper(null);
//        // 是否生成
////        templateConfig.setService(null);
////        templateConfig.setServiceImpl(null);
//        mpg.setTemplate(templateConfig);
//        mpg.setTemplateEngine(new FreemarkerTemplateEngine());
//
//        // 策略配置
//        StrategyConfig strategy = new StrategyConfig();
//        // 此处可以修改为您的表前缀,可去掉自己想去掉的表前缀
//        strategy.setTablePrefix("t_");
//        // 表名生成策略,驼峰命名法
//        strategy.setNaming(NamingStrategy.underline_to_camel);
//        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
//        // 需要生成的表
//        strategy.setInclude(tables);
//        // 排除生成的表
//        //strategy.setExclude(new String[]{"activity_log","t_log","user_activity"});
//        //开启实体类的lombok注解
//        strategy.setEntityLombokModel(true);
//        strategy.setRestControllerStyle(false);
//        // 在实体类中移除is前缀
//        strategy.setEntityBooleanColumnRemoveIsPrefix(true);
//        mpg.setStrategy(strategy);
//        // 包配置
//        PackageConfig pc = new PackageConfig();
//        //这里写该目录下的包名
//        pc.setParent(parentPackage);
//        pc.setEntity("dataobject");
//        pc.setMapper("mapper");
//        //服务层的接口以及实现类
//        pc.setService("service");
//        pc.setController("vo");
//        pc.setServiceImpl("service.impl");
//        mpg.setPackageInfo(pc);
//        // 执行生成
//        mpg.execute();
//        System.out.println("生成成功...");
//    }
//}
