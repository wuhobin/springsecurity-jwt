package com.wuhobin.springsecurity.controller;

import com.wuhobin.springsecurity.cache.SmsCodeCache;
import com.wuhobin.springsecurity.common.api.CommonResult;
import com.wuhobin.springsecurity.common.api.ResultCode;
import com.wuhobin.springsecurity.service.SmsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wuhongbin
 * @description: 验证码处理器
 * @datetime 2023/01/17 11:11
 */
@Api(value = "手机号验证码接口",tags = {"手机号验证码接口"})
@RestController
@RequestMapping("/api/code")
@Slf4j
public class SmsController {

    @Autowired
    private SmsCodeCache smsCodeCache;

    @Autowired
    private SmsService smsService;

    /**
     * 连续三次验证码错误限制登录
     */
    private static final int RESTRICTION_TIMES = 3;



    /**
     * 发送验证码
     * @return
     */
    @ApiOperation(value = "发送验证码", notes = "(/api/code/send)", httpMethod = "POST")
    @RequestMapping(value = "send", method = RequestMethod.POST)
    public CommonResult sendCode(String mobile){
        try {
            if (StringUtils.isBlank(mobile)){
                log.error("参数校验失败，mobile为空");
                return CommonResult.failed(ResultCode.VALIDATE_FAILED);
            }
            //判断该用户验证码连续错误次数
            Integer verifyCodeErr = smsCodeCache.getVerifyCodeErr(mobile);
            if (verifyCodeErr >= RESTRICTION_TIMES) {
                log.error("【登录模块】，验证码连续错误三次，两小时内禁止登录!,mobile={}",mobile);
                return CommonResult.failed(ResultCode.ASTRICT_LOGIN);
            }
            //获取限制发送
            String verifyCode = smsCodeCache.getMobileLatter(mobile);
            if (StringUtils.isNotBlank(verifyCode)) {
                log.info("【登录模块】，验证码发送限制时间还未到，不允许发送验证码!,mobile={}", mobile);
                return CommonResult.failed(ResultCode.PLEASE_TRY_LATER);
            }
            String code = smsService.sendCode(mobile);
            if (StringUtils.isBlank(code)){
                log.error("【登录模块】，验证码生成为空，请重新再试!,mobile={}", mobile);
                return CommonResult.failed(ResultCode.SEND_VERIFY_CODE_ERR);
            }
            smsCodeCache.setVerifyCode(mobile,code);
            smsCodeCache.setMobileLatter(mobile);
            return CommonResult.success();
        }catch (Exception e){
            log.error("【登录模块】，发送验证码错误！mobile={}", mobile, e);
            return CommonResult.failed(ResultCode.SEND_VERIFY_CODE_ERR);
        }
    }






}
