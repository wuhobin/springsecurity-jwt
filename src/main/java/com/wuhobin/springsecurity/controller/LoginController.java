package com.wuhobin.springsecurity.controller;

import com.wuhobin.springsecurity.cache.SmsCodeCache;
import com.wuhobin.springsecurity.cache.TokenCache;
import com.wuhobin.springsecurity.common.api.CommonResult;
import com.wuhobin.springsecurity.common.api.ResultCode;
import com.wuhobin.springsecurity.config.security.mobile.MobileCodeAuthenticationToken;
import com.wuhobin.springsecurity.service.UserService;
import com.wuhobin.springsecurity.util.JwtUtils;
import com.wuhobin.springsecurity.vo.UserVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jdk.nashorn.internal.parser.Token;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;


/**
 * @author wuhongbin
 * @description: 登录
 * @datetime 2023/01/11 15:19
 */
@Api(value = "用户接口",tags = {"用户接口"})
@RestController
@RequestMapping("/api/user")
@Slf4j
public class LoginController {

    @Autowired
    private TokenCache tokenCache;

    @Autowired
    private SmsCodeCache smsCodeCache;

    @Autowired
    private AuthenticationManager authenticationManager;

    /**
     * 连续三次验证码错误限制登录
     */
    private static final int RESTRICTION_TIMES = 3;

    @ApiOperation("用户名密码登录")
    @PostMapping("/login")
    public CommonResult login(String username,String password){
        if (StringUtils.isBlank(username) || StringUtils.isBlank(password)){
            log.info("【用户登录】 用户名或者密码为空");
            return CommonResult.failed(ResultCode.VALIDATE_FAILED);
        }
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username, password);
        Authentication authenticate = authenticationManager.authenticate(authenticationToken);
        if (ObjectUtils.isEmpty(authenticate)){
            return CommonResult.failed(ResultCode.ACCOUNT_ERR);
        }
        UserVO userVO = (UserVO) authenticate.getPrincipal();
        String token = JwtUtils.generateJWT(userVO.getUserId(),userVO.getUsername());
        if (StringUtils.isEmpty(token)) {
            log.error("【用户登录】- 用户登录token为空! userId = {}", userVO.getUserId());
            return CommonResult.failed(ResultCode.LOGIN_FAIL);
        }
        tokenCache.setToken(userVO.getUserId(),token);
        Map<String, Object> map = new HashMap<>(4);
        map.put("token",token);
        return CommonResult.success(map);
    }

    @ApiOperation("手机号验证码登录")
    @PostMapping("/mobile-login")
    public CommonResult mobileLogin(String mobile,String code){
        if (StringUtils.isBlank(mobile) || StringUtils.isBlank(code)){
            log.info("【用户登录】 手机号或者验证码为空");
            return CommonResult.failed(ResultCode.VALIDATE_FAILED);
        }
        //判断该用户验证码连续错误次数
        Integer verifyCodeErr = smsCodeCache.getVerifyCodeErr(mobile);
        if (verifyCodeErr >= RESTRICTION_TIMES) {
            log.error("【用户登录】，验证码连续错误三次，两小时内禁止登录!");
            return CommonResult.failed(ResultCode.ASTRICT_LOGIN);
        }
        MobileCodeAuthenticationToken mobileCodeAuthenticationToken = new MobileCodeAuthenticationToken(mobile);
        Authentication authenticate = authenticationManager.authenticate(mobileCodeAuthenticationToken);
        if (ObjectUtils.isEmpty(authenticate)){
            return CommonResult.failed(ResultCode.SEND_VERIFY_ERR);
        }
        UserVO userVO = (UserVO) authenticate.getPrincipal();
        String token = JwtUtils.generateJWT(userVO.getUserId(),userVO.getUsername());
        if (StringUtils.isEmpty(token)) {
            log.error("【用户登录】- 用户登录token为空! userId = {}", userVO.getUserId());
            return CommonResult.failed(ResultCode.LOGIN_FAIL);
        }
        tokenCache.setToken(userVO.getUserId(),token);
        Map<String, Object> map = new HashMap<>(4);
        map.put("token",token);
        return CommonResult.success(map);
    }




    @ApiOperation("测试")
    @GetMapping("/test")
    public CommonResult test(){
        return CommonResult.success();
    }


}
