package com.wuhobin.springsecurity.constant;

/**
 * 消息常量
 *
 * @author Honghui [wanghonghui_work@163.com] 2021/3/16
 */
public class MessageConstant {


    public static final String USER_DISABLE_ERROR = "用户不存在";
    public static final String USER_NO_REGISTER = "用户暂未注册";
    public static final String VERIFY_CODE_ERROR = "验证码错误！";
    public static final String VERIFY_CODE_NOT_CLICK = "请先获取验证码";
    public static final String MSG_NOT_LOGIN = "未登录，请重新登录！";

}
