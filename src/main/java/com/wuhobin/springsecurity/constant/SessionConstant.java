package com.wuhobin.springsecurity.constant;

/**
 * Session常量
 * @author 刘博
 */
public class SessionConstant {

    /**
     * app session
     */
    public static final String SESSION_APP_USER = "jwt-app";
    /**
     * WeChat session
     */
    public static final String SESSION_WE_CHAT_USER = "huaer-weChat";
    /**
     * 后台admin session
     */
    public static final String SESSION_ADMIN_USER = "cmb-convenience_pray_admin";

}
